import React, { Component } from 'react';
import web3 from "./web3";
import Election from "./election";
import {
    Badge,
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupButtonDropdown,
    InputGroupText,
    Label,
    Row,
} from 'reactstrap';

class Forms extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.toggleFade = this.toggleFade.bind(this);
        this.state = {
            collapse: true,
            fadeIn: true,
            timeout: 300,
            owner: "",
            candidates1: [],
            candidates2: []
        };
    }

    async componentDidMount() {
        const owner = await Election.methods.owner().call();
        const candidates1 = await Election.methods.candidates("0").call();
        const candidates2 = await Election.methods.candidates("1").call();
        const totalVotes = await Election.methods.totalVotes().call();
        this.setState({ owner, candidates1, candidates2, totalVotes });
    }

    onSubmit = async event => {
        event.preventDefault();

        const accounts = await web3.eth.getAccounts();

        this.setState({ message: "Waiting on transaction success..." });

        await Election.methods.vote(this.state.value).send({
            from: accounts[0]
        });

        this.setState({ message: "You chose " });
    };

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }

    toggleFade() {
        this.setState((prevState) => { return { fadeIn: !prevState } });
    }

    // onClick = async () => {
    //     const accounts = await web3.eth.getAccounts();
    //     const response = await Election.methods.getArray().call();
    //     this.setState({ message: response });
    //   };
    onClick = async () => {
        const response = await Election.methods.getArray().call();
        const responser = response.toString().replace("0x" ," 0x");
        this.setState({ message: responser });
    };

    render() {
        return (
            <div className="animated fadeIn">
                <Col xs="12" md="6" style={{ paddingTop: '30px', paddingLeft: '3px' }} >
                    <Card >
                        <CardHeader>
                            <strong>Voter</strong>
                        </CardHeader>
                        <CardBody>
                            <Form action="" method="post" className="form-horizontal">
                                <FormGroup row>
                                    {/* <Col md="10">
                                        <Label htmlFor="hf-email">These are the candidates :</Label>
                                        <Label htmlFor="hf-email">0: <strong>{this.state.candidates1.name}</strong>  {this.state.candidates1.count}{" "}</Label>
                                        <Label htmlFor="hf-email">1: <strong>{this.state.candidates2.name}</strong> </Label>
                                    </Col> */}
                                    <Col md="10">
                                        These are the candidates :
                                    </Col>
                                    <Row></Row>
                                    <Col md="10">
                                        0: <strong>{this.state.candidates1.name}</strong>  {this.state.candidates1.count}{" "}
                                    </Col>
                                    <Col md="10">
                                        1: <strong>{this.state.candidates2.name}</strong>
                                    </Col>
                                    <Col md="10">
                                        You can Choose your according to your candidates name.
                                    </Col>

                                    {/* <Col>
                                        <table class="table " style={{ marginTop: '5%' }}>
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Hashcode</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">*</th>
                                                    <td>{this.state.message}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </Col> */}
                                </FormGroup>

                                <FormGroup row>
                                    <Col>
                                        <p style={{wordSpacing: "30px"}}>{this.state.message}</p>
                                        <button type="button" onClick={this.onClick}>Show!</button>
                                    </Col>
                                </FormGroup>
                            </Form>
                        </CardBody>
                        {/* <CardFooter>
                            <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                            <Button type="reset" size="sm" color="danger" style={{ marginLeft: '10px' }}><i className="fa fa-ban"></i> Reset</Button>
                        </CardFooter> */}
                    </Card>
                </Col>

            </div>
        );
    }
}

export default Forms;