import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, ModalHeader, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Label } from 'reactstrap';
import { AvForm, AvField, AvGroup, AvInput, AvFeedback, AvRadioGroup, AvRadio, AvCheckboxGroup, AvCheckbox } from 'availity-reactstrap-validation';
// import ForgotPassword from './ForgotPassword';
// import Register2 from './Register';
import Logo from '../../assets/img/brand/favicon.png';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Election from "./election";
import web3 from "./web3";
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "",
      message2: "",
      role: ""
      //   userName: '',
      //   password: '',
      //   userType: 'TEACHER',
      //   response: null
    }
  }

  toggleForgotPassword = () => {
    console.log('forgot pass');
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }))
  }

  // getRole = async () => {
  //   const accounts = await web3.eth.getAccounts();
  //   const response = await Election.methods
  //     .validateVoterOrOwner()
  //     .call({ from: accounts[0] });
  //   this.setState({ role: response });
  //   localStorage.setItem("role", response);
  //   this.props.history.push('/DashboardBlock')
  // };

  render() {

    return (
      <div className="app flex-row align-items-center">
        <Container style={{ width: "700px" }}>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4" style={{ borderRadius: "20px" }}>
                  <CardBody>
                    <AvForm >
                    <img style={{ width: '150px', height: '60px', 'margin-left': '28%', 'margin-right': 'auto', 'margin-bottom': 'px' }} src={Logo} alt="logo" />
                      <AvGroup style={{ marginTop: "0rem" }}>
                        <Label className="avenir-black-primary" for="desc">
                          Input Your Address
                        </Label>
                        <AvInput required type="text" name="u_name"
                          validate={{
                            required: { value: true },
                            pattern: { value: '^[A-Za-z" "]+$' }
                          }} placeholder="Write Name" 
                          
                          style={{'border-radius': '10px'}}
                          />
                          
                      </AvGroup>

                      <AvGroup>
                        <Button
                          color="primary"
                          className="px-4"
                          type="submit"
                          style={{ fontSize: 12 }}>
                          Submit
                        </Button>
                      </AvGroup>



                      {/* <Row style={{ paddingTop: '30px' }}>
                        <Button
                          style={{ 'background': '#1E90FF', 'borderRadius': '30px', 'color': 'white', width: '100%' }}
                          onClick={this.getRole}>
                          <strong>Login</strong>
                        </Button>
                        <p>{this.state.role}</p> */}

                      {/* <Col>
                            <Button
                              type="submit"
                              style={{ 'background': '#1E90FF', 'borderRadius': '30px', 'color': 'white', width: '100%' }}
                              className="px-4"
                              className="px-0" onClick={() => this.props.history.push('/Voter')}
                            >As Voter</Button>

                          </Col>
                          <Col>
                            <Button
                              type="submit"
                              style={{ 'background': '#1E90FF', 'borderRadius': '30px', 'color': 'white', width: '100%' }}
                              className="px-4"
                              className="px-0" onClick={() => this.props.history.push('/DashboardBlock')}
                            >As Owner</Button>

                          </Col> */}
                      {/* </Row> */}

                    </AvForm>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
