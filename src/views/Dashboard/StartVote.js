import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, ModalHeader, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Label } from 'reactstrap';
import { AvForm, AvField, AvGroup, AvInput, AvFeedback, AvRadioGroup, AvRadio, AvCheckboxGroup, AvCheckbox } from 'availity-reactstrap-validation';
// import ForgotPassword from './ForgotPassword';
// import Register2 from './Register';
import Logo from '../../assets/img/brand/favicon.png';
import Countdown from 'react-countdown-now';
import Clock from './Clock';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deadLine:'Februari 28,2020',
            newDeadLine:'',
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0
        }
    }

    changeDeadLine(){
        // console.log('state',this.state);
        this.setState({deadLine: this.state.newDeadLine});
    }


    // getRole = async () => {
    //   const accounts = await web3.eth.getAccounts();
    //   const response = await Election.methods
    //     .validateVoterOrOwner()
    //     .call({ from: accounts[0] });
    //   this.setState({ role: response });
    //   localStorage.setItem("role", response);
    //   this.props.history.push('/DashboardBlock')
    // };

    render() {
       
        return (
            <div className="app flex-row align-items-center">
                <Container style={{ width: "700px" }}>
                    <Row className="justify-content-center">
                        <Col md="8">
                            <CardGroup>
                                <Card className="p-4" style={{ borderRadius: "20px" }}>
                                    <CardBody>
                                        <AvForm >

                                            <img
                                                style={{
                                                    width: '150px', height: '60px', 'margin-left': '28%',
                                                    'margin-right': 'auto', 'margin-bottom': 'px'
                                                }}
                                                src={Logo} alt="logo" />
                                            <div>
                                                <div style={{ textAlign: "center" }}>
                                                   <strong>Countdown to {this.state.deadLine}</strong>
                                                    </div>

                                                <Clock
                                                deadLine={this.state.deadLine}
                                                />

                                                {/* <div style={{textAlign:'center', marginBottom:'10px'}}>
                                                    <input 
                                                    placeholder='new date'
                                                    onChange={event => this.setState({newDeadLine: event.target.value})}
                                                    />
                                                    <button onClick={() =>this.changeDeadLine()}>
                                                        submit</button>
                                                </div> */}

                                            </div>


                                            {/* <div style={{ paddingLeft: '47%' }}>
                                                <Countdown
                                                    date={Date.now() + 10000}
                                                    intervalDelay={0}
                                                    precision={3}
                                                    renderer={props => <div>{props.total}</div>}
                                                />
                                            </div> */}

                                            <AvGroup>
                                                <div style={{ paddingLeft: '40%', paddingTop:'10px' }}>
                                                    <Button
                                                        color="primary"
                                                        className="px-4"
                                                        type="submit"
                                                        style={{ fontSize: 12 }}
                                                        onClick={() => this.props.history.push('/LoginBaru')}>
                                                        Start
                                                </Button>
                                                
                                                </div>
                                            </AvGroup>

                                        </AvForm>
                                    </CardBody>
                                </Card>
                            </CardGroup>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Login;
