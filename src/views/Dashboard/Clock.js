import React, { Component } from 'react';


class Clock extends Component {

    constructor(props) {
        super(props);
        this.state = {
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0
        }
    }

    componentWillMount() {
        this.getTimeUntil(this.props.deadLine);
    }

    componentWillMount() {
        setInterval(() => this.getTimeUntil(this.props.deadLine), 1000)
    }

    Leading0(num) {
        return num < 10 ? '0' + num : num;
    }

    getTimeUntil(deadLine) {
        const time = Date.parse(deadLine) - Date.parse(new Date());
        console.log('time', time);
        const seconds = Math.floor((time / 1000) % 60);
        const minutes = Math.floor((time / 1000 / 60) % 60);
        const hours = Math.floor(time / (1000 * 60 * 60) % 24);
        const days = Math.floor(time / (1000 * 60 * 60 * 24));

        console.log('seconds', seconds, 'minutes', minutes, 'hours', hours, 'days', days);
        this.setState({ days, hours, minutes, seconds });
    }
    render() {
        return (

            <div style={{ textAlign: "center", fontSize: "15px" }}>
                <div style={{ display: "inLine", margin: '5px' }}>
                    <strong>{this.Leading0(this.state.days)} days</strong></div>
                <div style={{ display: "inLine", margin: '5px' }}>
                    <strong>{this.Leading0(this.state.hours)} hours</strong></div>
                <div style={{ display: "inLine", margin: '5px' }}>
                    <strong>{this.Leading0(this.state.minutes)} minutes</strong></div>
                <div style={{ display: "inLine", margin: '5px' }}>
                    <strong>{this.Leading0(this.state.seconds)} seconds</strong></div>
            </div>
        )
    }
} export default Clock